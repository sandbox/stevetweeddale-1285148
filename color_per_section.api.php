<?php

/**
 * Define css templates to use with color_per_section module.
 *
 * Implement this hook to provide a css template file you wish to duplicate for
 * each 'section' of the site. The expected.
 *
 * @return
 *   An array of templates. Each template has a key that forms a machine-name
 *   for the template. The corresponding array value is and associative array of
 *   2 required elements:
 *   - "file": The full path to the css template file.
 *   - "placeholders": An associative array with 2 elements:
 *     - "section": The token used in the template to denote the 'section'
 *     - "colors": An associative array of colors and the tokens used to denote
 *       them in the css file.
 */
function HOOK_color_per_section_default_templates() {
  return array(
    'section_colours' => array(
      'file' => drupal_get_path('module', 'my_module') . '/section_colours.css',
      'placeholders' => array(
        'section' => '#section#',
        'colors' => array(
          'dark' => '#color-dark#',
          'light' => '#color-light#',
        ),
      ),
    ),
  );
}

/**
 * Declare the replacement values for an elsewhere-declared css template.
 *
 * @param $key
 *   A string containing the template machine-name.
 * @return
 *   An array of sections, each containing an associative array of colors, the
 *   key of which corresponds to the key given in the
 *   hook_color_per_section_default_templates().
 */
function HOOK_color_per_section_template_tokens($key) {
  $sections = array();
  if ($key == 'campaign_colours') {
    $sections = array(
      'organisation-and-culture' => array(
        'dark' => '#669933',
        'light' => '#E2ECBB',
      ),
    );
  }
  return $sections;
}